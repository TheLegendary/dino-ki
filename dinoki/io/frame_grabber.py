"""
This class implements a frame grabber.

When run, it yields a constant stream of stills from the screen.

Example:
    fg = FrameGrabber()
    for _ in range(10):
        f = fg.get_frame()
        do_something_with(f)
"""

from PIL import ImageGrab
import numpy as np
from ctypes import windll

class FrameGrabber(object):

    def get_frame(self):
        user32 = windll.user32
        user32.SetProcessDPIAware() 
        img = ImageGrab.grab(bbox=(450, 150, 1450, 450)) #bbox specifies specific region (bbox= x,y,width,height)
        img_np = np.array(img)
        return img_np.tolist() #the 'tolist()'-function converts everything into an array
